package com.example.amartineza.loginschedules;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {
    EditText etUserLogin;
    EditText etPassLogin;
    Button btnLogin;
    String userLogin;
    String userPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etUserLogin = (EditText) findViewById(R.id.etUserNameLogin);
        etPassLogin = (EditText) findViewById(R.id.etPassLogin);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userLogin = etUserLogin.getText().toString();
                userPass = etPassLogin.getText().toString();
                if (validateForm()) {
                    callLoginService(userLogin, userPass);
                }
            }
        });
    }

    private void callLoginService(String userLogin, String userPass) {
        String ENDPOINT = getString(R.string.host_api_stage);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ENDPOINT).addConverterFactory(GsonConverterFactory.create()).build();

        final LoginRequest modelRequest = new LoginRequest();
        modelRequest.setCountryCode(getString(R.string.countryCode));
        modelRequest.setUsername(userLogin);
        modelRequest.setPassword(userPass);
        modelRequest.setGrantType(getString(R.string.grantType));
        modelRequest.setClientId(getString(R.string.clientId));
        modelRequest.setClientSecret(getString(R.string.clientSecret));

        Services sporaServices = retrofit.create(Services.class);

        sporaServices.getLogin(modelRequest.getCountryCode(),
                modelRequest.getUsername(),
                modelRequest.getPassword(),
                modelRequest.getGrantType(),
                modelRequest.getClientId(),
                modelRequest.getClientSecret()).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    String accessToken = response.body().getAccessToken();
                    String tokenType = response.body().getTokenType();
                    String token = tokenType + " " + accessToken;
                    Intent intent = new Intent(LoginActivity.this, UserDataActivity.class);
                    intent.putExtra("token",token);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(LoginActivity.this, "Usuario y/o Contraseña incorrectos", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("", "onFailure");
            }
        });
    }

    public Boolean validateForm() {
        Boolean itsValid = true;
        if (etUserLogin.getText().toString().isEmpty()) {
            etUserLogin.setError("Introduce Usuario");
            itsValid = false;
        }
        if (etPassLogin.getText().toString().isEmpty()) {
            etPassLogin.setError("Introduce Contraseña");
            itsValid = false;
        }

        return itsValid;
    }

}

package com.example.amartineza.loginschedules;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserDataActivity extends AppCompatActivity {
    List<DataResponse.transaction> list = new ArrayList<>();
    TextView tvNameData;
    TextView tvEmailData;
    TextView tvLevelData;
    EditText etCardNumber;

    RecyclerView rvData;
    DataAdapter dataAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_data);
        rvData = findViewById(R.id.dataRecyler);
        tvNameData = (TextView) findViewById(R.id.tvNameData);
        tvEmailData = (TextView) findViewById(R.id.tvEmailData);
        tvLevelData = (TextView) findViewById(R.id.tvLevelData);
        etCardNumber = (EditText) findViewById(R.id.etCardNumber);

        etCardNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etCardNumber.getText().length() == 16) {
                    String cardNumber = etCardNumber.getText().toString();
                    Intent intent = getIntent();
                    String token = intent.getStringExtra("token");
                    callRetrofitService(token, cardNumber);
                }
            }
        });
    }

    private void callRetrofitService(String token, String cardNumber) {
        String ENDPOINT = getString(R.string.host_api_loyalti);
        String apiKey = getString(R.string.apiKey);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ENDPOINT).addConverterFactory(GsonConverterFactory.create()).build();

        DataRequest modelRequest = new DataRequest();
        modelRequest.setCardNumber(cardNumber); //1303030317482433
        modelRequest.setCountryCode(getString(R.string.countryCodeLoyalti));
        modelRequest.setTransactionInclue(true);

        Services services = retrofit.create(Services.class);

        services.getMembersLoyalty(apiKey, token, modelRequest).enqueue(new Callback<DataResponse>() {
            @Override
            public void onResponse(Call<DataResponse> call, Response<DataResponse> response) {
                if (response.isSuccessful()) {
                    tvNameData.setText(response.body().getName());
                    tvEmailData.setText(response.body().getEmail());
                    tvLevelData.setText(response.body().getLevel().getNextLevel());

                    list = response.body().getTransactions();
                    dataAdapter = new DataAdapter(getApplicationContext(), list);
                    rvData.setAdapter(dataAdapter);
                    rvData.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                    rvData.hasFixedSize();

                } else {
                    tvNameData.setText("");
                    tvEmailData.setText("");
                    tvLevelData.setText("");
                    list.clear();
                    dataAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<DataResponse> call, Throwable t) {
                Log.e("", "onFailure");
            }
        });
    }
}

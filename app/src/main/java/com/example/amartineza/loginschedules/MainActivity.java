package com.example.amartineza.loginschedules;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    Button btnGoLogin;EditText etCityId;
    ImageView ivPosterMovie;
    TextView tvNameMovie;
    TextView tvSynopsis;
    TextView tvDateMovie;
    TextView tvDateTime;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnGoLogin = (Button)findViewById(R.id.btnGoLogin);
        etCityId = (EditText) findViewById(R.id.etCityId);
        ivPosterMovie = (ImageView) findViewById(R.id.ivPosterMovie);
        tvNameMovie = (TextView) findViewById(R.id.tvNameMovie);
        tvSynopsis = (TextView) findViewById(R.id.tvSynopsisMovie);
        tvDateMovie = (TextView) findViewById(R.id.tvDateMovie);
        tvDateTime = (TextView) findViewById(R.id.tvDateTime);
        etCityId.setText("32");
        callServiceSchedules();

        btnGoLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    private void callServiceSchedules() {
        String ENDPOINT = getString(R.string.host_api_stage_schedules);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ENDPOINT).addConverterFactory(GsonConverterFactory.create()).build();

        String countryCode = "ES";
        boolean includeCinemas = true;
        boolean includeMovies = true;
        int cities = 32;

        Services services = retrofit.create(Services.class);
        services.getSchudels(countryCode, cities, includeCinemas, includeMovies).enqueue(new Callback<SchedulesModel>() {
            @Override
            public void onResponse(Call<SchedulesModel> call, Response<SchedulesModel> response) {
                if (response.isSuccessful()) {
                    String name = response.body().getMovies().get(1).getName();
                    tvNameMovie.setText(name);

                    String synopsis = response.body().getMovies().get(1).getSynopsis();
                    tvSynopsis.setText(synopsis);

                    String resource = response.body().getMovies().get(1).getMedia().get(0).getResource();
                    String route = response.body().getRoutes().get(1).getSizes().getLarge();
                    Picasso.get().load(route + "" + resource).into(ivPosterMovie);


                    int movieId = response.body().getMovies().get(1).getId();
                    int sizeIdMovie = response.body().getSchedules().size();
                    String date = "2018-04-26";

                    for (int i = 0; i < sizeIdMovie; i++) {
                        int movieIdResponse = response.body().getSchedules().get(i).getMovieId();
                        if (movieId == movieIdResponse) {
                            int sizeDateMovie = response.body().getSchedules().get(i).getDates().size();
                            for (int j = 0; j < sizeDateMovie; j++) {
                                String dateResponse = response.body().getSchedules().get(i).getDates().get(j).getDate();
                                if (date.equals(dateResponse)) {
                                    String dateTime = response.body().getSchedules().get(i).getDates().get(j)
                                            .getFormats().get(0).getShowtimes().get(0).getDatetime();
                                    SimpleDateFormat parseador = new SimpleDateFormat("yyyy-mm-dd'T'HH:mm:ss");
                                    SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd H:mm a");
                                    try {
                                        Date finalDate = parseador.parse(dateTime);
                                        tvDateTime.setText(formateador.format(finalDate));
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SchedulesModel> call, Throwable t) {
                Log.e("fail", t.getMessage());
            }
        });
    }


}

package com.example.amartineza.loginschedules;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by amartineza on 4/9/2018.
 */

public class DataResponse {

    private String name;
    private String email;
    @SerializedName("balance_list")
    private List<balance> balanceList;
    private level level;

    private String message;
    @SerializedName("status_code")
    private int statusCode;

    public class balance {
        private Double balance;
        private String key;
        private String name;
        private String message;

        public Double getBalance() {
            return balance;
        }

        public void setBalance(Double balance) {
            this.balance = balance;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public class level {
        @SerializedName("next_level")
        private String nextLevel;
        @SerializedName("advance_percent")
        private double advancePercent;
        private String key;
        private String name;
        private String message;

        public String getNextLevel() {
            return nextLevel;
        }

        public void setNextLevel(String nextLevel) {
            this.nextLevel = nextLevel;
        }

        public double getAdvancePercent() {
            return advancePercent;
        }

        public void setAdvancePercent(double advancePercent) {
            this.advancePercent = advancePercent;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    private List<transaction> transactions;

    public class transaction {
        private String cinema;
        private String message;
        private String date;
        private int points;

        public String getCinema() {
            return cinema;
        }

        public void setCinema(String cinema) {
            this.cinema = cinema;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public int getPoints() {
            return points;
        }

        public void setPoints(int points) {
            this.points = points;
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<balance> getBalanceList() {
        return balanceList;
    }

    public void setBalanceList(List<balance> balanceList) {
        this.balanceList = balanceList;
    }

    public DataResponse.level getLevel() {
        return level;
    }

    public void setLevel(DataResponse.level level) {
        this.level = level;
    }

    public List<transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<transaction> transactions) {
        this.transactions = transactions;
    }
}

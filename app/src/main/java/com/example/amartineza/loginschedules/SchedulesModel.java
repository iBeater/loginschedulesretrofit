package com.example.amartineza.loginschedules;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amartineza on 4/9/2018.
 */

public class SchedulesModel {

    private List<Movies> movies;

    public class Movies {
        private int id;
        private String name;
        private String code;
        private String rating;
        private String length;
        private String synopsis;
        private String genre;
        private List<Media> media;
        private int position;
        private List<String> categories = new ArrayList<>();

        public class Media {
            private String type;
            private String code;
            private String resource;

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getResource() {
                return resource;
            }

            public void setResource(String resource) {
                this.resource = resource;
            }
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getLength() {
            return length;
        }

        public void setLength(String length) {
            this.length = length;
        }

        public String getSynopsis() {
            return synopsis;
        }

        public void setSynopsis(String synopsis) {
            this.synopsis = synopsis;
        }

        public String getGenre() {
            return genre;
        }

        public void setGenre(String genre) {
            this.genre = genre;
        }

        public List<Media> getMedia() {
            return media;
        }

        public void setMedia(List<Media> media) {
            this.media = media;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public List<String> getCategories() {
            return categories;
        }

        public void setCategories(List<String> categories) {
            this.categories = categories;
        }
    }

    public List<Movies> getMovies() {
        return movies;
    }

    public void setMovies(List<Movies> movies) {
        this.movies = movies;
    }

    private List<Routes> routes;

    public class Routes {
        private String code;
        private Sizes sizes;

        public class Sizes {
            private String large;
            private String medium;
            private String small;
            @SerializedName("x-large")
            private String xlarge;

            public String getLarge() {
                return large;
            }

            public void setLarge(String large) {
                this.large = large;
            }

            public String getMedium() {
                return medium;
            }

            public void setMedium(String medium) {
                this.medium = medium;
            }

            public String getSmall() {
                return small;
            }

            public void setSmall(String small) {
                this.small = small;
            }

            public String getXlarge() {
                return xlarge;
            }

            public void setXlarge(String xlarge) {
                this.xlarge = xlarge;
            }
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public Sizes getSizes() {
            return sizes;
        }

        public void setSizes(Sizes sizes) {
            this.sizes = sizes;
        }
    }

    private List<Schedules> schedules;

    public class Schedules {
        @SerializedName("movie_id")
        private int movieId;
        @SerializedName("cinema_id")
        private int cinemaId;
        @SerializedName("city_id")
        private int cityId;
        @SerializedName("is_especial_prices")
        private boolean isEspecialPrices;
        private List<DatesSchudles> dates;

        public class DatesSchudles {
            private String date;
            private List<FormatsSchudles> formats;

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public List<FormatsSchudles> getFormats() {
                return formats;
            }

            public void setFormats(List<FormatsSchudles> formats) {
                this.formats = formats;
            }

            public class FormatsSchudles {
                @SerializedName("format_id")
                private int formatId;
                @SerializedName("vista_id")
                private String vistaId;
                private String lenguage;
                private List<Showtimes> showtimes;

                public class Showtimes {
                    @SerializedName("vista_id")
                    private String vistaId;
                    private String datetime;
                    private int screen;
                    //private List<String> settings;
                    @SerializedName("early_morning")
                    private boolean earlyMorning;

                    public String getVistaId() {
                        return vistaId;
                    }

                    public void setVistaId(String vistaId) {
                        this.vistaId = vistaId;
                    }

                    public String getDatetime() {
                        return datetime;
                    }

                    public void setDatetime(String datetime) {
                        this.datetime = datetime;
                    }

                    public int getScreen() {
                        return screen;
                    }

                    public void setScreen(int screen) {
                        this.screen = screen;
                    }

                    public boolean isEarlyMorning() {
                        return earlyMorning;
                    }

                    public void setEarlyMorning(boolean earlyMorning) {
                        this.earlyMorning = earlyMorning;
                    }
                }

                public int getFormatId() {
                    return formatId;
                }

                public void setFormatId(int formatId) {
                    this.formatId = formatId;
                }

                public String getVistaId() {
                    return vistaId;
                }

                public void setVistaId(String vistaId) {
                    this.vistaId = vistaId;
                }

                public String getLenguage() {
                    return lenguage;
                }

                public void setLenguage(String lenguage) {
                    this.lenguage = lenguage;
                }

                public List<Showtimes> getShowtimes() {
                    return showtimes;
                }

                public void setShowtimes(List<Showtimes> showtimes) {
                    this.showtimes = showtimes;
                }
            }
        }

        public int getMovieId() {
            return movieId;
        }

        public void setMovieId(int movieId) {
            this.movieId = movieId;
        }

        public int getCinemaId() {
            return cinemaId;
        }

        public void setCinemaId(int cinemaId) {
            this.cinemaId = cinemaId;
        }

        public int getCityId() {
            return cityId;
        }

        public void setCityId(int cityId) {
            this.cityId = cityId;
        }

        public boolean isEspecialPrices() {
            return isEspecialPrices;
        }

        public void setEspecialPrices(boolean especialPrices) {
            isEspecialPrices = especialPrices;
        }

        public List<DatesSchudles> getDates() {
            return dates;
        }

        public void setDates(List<DatesSchudles> dates) {
            this.dates = dates;
        }
    }

    private List<String> dates;

    public List<Routes> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Routes> routes) {
        this.routes = routes;
    }

    public List<Schedules> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<Schedules> schedules) {
        this.schedules = schedules;
    }

    public List<String> getDates() {
        return dates;
    }

    public void setDates(List<String> dates) {
        this.dates = dates;
    }
}

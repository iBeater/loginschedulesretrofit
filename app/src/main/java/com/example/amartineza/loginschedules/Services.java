package com.example.amartineza.loginschedules;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by amartineza on 4/9/2018.
 */

public interface Services {

    @Headers("api_key:es_cinepolis_test_android")
    @GET("v2/schedules")
    Call<SchedulesModel> getSchudels(@Query("country_code") String countryCode,
                                     @Query("cities") int cities,
                                     @Query("include_cinemas") boolean includeCinemas,
                                     @Query("include_movies") boolean includeMovies);

    @FormUrlEncoded
    @Headers({"api_key:cinepolis_test_android",
            "Content-Type:application/x-www-form-urlencoded"})
    @POST("v2/oauth/token")
    Call<LoginResponse> getLogin(@Field("country_code") String contryCode,
                                 @Field("username") String userName,
                                 @Field("password") String pass,
                                 @Field("grant_type") String grantType,
                                 @Field("client_id") String clientId,
                                 @Field("client_secret") String clientSecret);

    @POST("v1/members/loyalty")
    Call<DataResponse> getMembersLoyalty(@Header("api_key") String apiKey ,
                                         @Header("Authorization") String token,
                                         @Body DataRequest bodyMembersLoyalti);
}
